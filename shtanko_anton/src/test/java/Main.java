public class Main {
    public static void main(String[] args) {
        FixedThreadPool pool = new FixedThreadPool(5);
        pool.start();
        System.out.println("FixedThreadPool task");
        for (int i = 0; i < 100; i++) {
            final int finalI = i;
            int finalI1 = i;
            pool.execute(() -> {
                double a = 1d;
                for (int j = 0; j < 50; j++) {
                    a *= Math.PI * Math.sqrt(Math.PI);
                }
                System.out.println(finalI1 + " " + Thread.currentThread().getName() + " Double a=" + a);
            });
        }

        pool.shutdown();
        System.out.println();

        System.out.println("ScalableThreadPool task");
        ScalableThreadPool scalableThreadPool = new ScalableThreadPool(2, 10);
        scalableThreadPool.start();
        for (int i = 0; i < 20; i++) {
            int finalI = i;
            scalableThreadPool.execute(() -> {
                System.out.println(finalI + " " + Thread.currentThread().getName() + " начал работу.");
            });
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        scalableThreadPool.shutdown();
    }
}
