import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class ScalableThreadPool implements ThreadPool {

    private final int minCountThread;
    private final int maxCountThread;
    private final Queue<Runnable> workQueue = new LinkedBlockingQueue<>();
    private volatile boolean isRunning = true;
    private final List<TaskWorker> threadList = new ArrayList<>();


    public ScalableThreadPool(int minCountThread, int maxCountThread) {
        this.minCountThread = minCountThread;
        this.maxCountThread = maxCountThread;
    }

    public void start() {
        for (int i = 0; i < minCountThread; i++) {
            TaskWorker thread = new TaskWorker();
            threadList.add(thread);
            thread.start();
        }
    }

    public void execute(Runnable runnable) {
        if (isRunning) {
            boolean threadBusy = true;
            for (TaskWorker thread : threadList) { //проверяю по всем потокам свободны они или нет, если хоть один свободен
                if (thread.isBusy()) {      //то соответственно меняю внутренний флаг threadBusy и не запускаю новые потоки
                    continue;
                } else {
                    threadBusy = false;
                    break;
                }
            }
            if (threadBusy) {
                for (int i = 0; i < maxCountThread - minCountThread; i++) {
                    TaskWorker thread = new TaskWorker();
                    threadList.add(thread);
                    thread.start();
                }
            }
            workQueue.offer(runnable);
        }
    }

    public void shutdown() {
        isRunning = false;
    }

    private final class TaskWorker extends Thread {

        private boolean isBusy;

        public boolean isBusy() {
            return isBusy;
        }

        private void setBusy(boolean busy) {
            isBusy = busy;
        }

        @Override
        public void run() {
            setBusy(true); //устанавливаю флаг, что поток не свободен
            Runnable nextTask;
            while (isRunning) {
                nextTask = workQueue.poll();
                if (nextTask != null) {
                    nextTask.run();
                    if (workQueue.isEmpty() && threadList.size() > minCountThread) {
                        threadList.remove(Thread.currentThread());
                    }
                    setBusy(false); //меняю флаг, что поток освободился
                }
            }
        }
    }
}
