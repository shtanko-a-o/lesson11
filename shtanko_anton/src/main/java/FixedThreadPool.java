
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool {

    private final int countThread;
    private final Queue<Runnable> workQueue = new LinkedBlockingQueue<>();
    private volatile boolean isRunning = true;

    public FixedThreadPool(int countThread) {
        this.countThread = countThread;
    }

    public void start() {
        for (int i = 0; i < countThread; i++) {
            Thread thread = new Thread(new TaskWorker());
            thread.start();
        }
    }

    public void execute(Runnable runnable) {
        if (isRunning) {
            synchronized (workQueue) {
                workQueue.offer(runnable);
            }
        }
    }

    public void shutdown() {
        isRunning = false;
    }

    private final class TaskWorker implements Runnable {
        @Override
        public void run() {
            Runnable nextTask;
            while (isRunning) {
                synchronized (workQueue) {
                    nextTask = workQueue.poll();
                    if (nextTask != null) {
                        nextTask.run();
                    }
                }
            }
        }
    }
}
